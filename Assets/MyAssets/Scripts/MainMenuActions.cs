﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading;


public class MainMenuActions : MonoBehaviour
{
    public Camera creditsCam;
    public Camera mainCam;
    public int creditsTime;

    private bool allowQuit = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplicationQuit()
    {
        creditsCam.enabled = true;
        mainCam.enabled = false;  
        StartCoroutine(WaitAndQuit());
    }

    IEnumerator WaitAndQuit()
    {
        yield return new WaitForSeconds(creditsTime);
        
        Application.Quit();
        if (Application.platform == RuntimePlatform.Android)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
            
    }

    public void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
