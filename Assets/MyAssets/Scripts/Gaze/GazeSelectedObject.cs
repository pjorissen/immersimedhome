﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeSelectedObject : MonoBehaviour
{
    // Start is called before the first frame update
    public Image UIImageWhereToShow;
    private GameObject attachedObject = null;

    public void Start()
    {
        attachedObject = null;
    }
    public GameObject GetAttachedObject()
    {
        return attachedObject;
    }
    public Sprite GetAttachedSprite()
    {
        return UIImageWhereToShow.sprite;
    }

    public void SetAttachedObject(GameObject selectedObject)
    {
        if(!attachedObject)
        {
            attachedObject = selectedObject;
            SetSprite(selectedObject.GetComponentInChildren<InventoryItem>().sprite);
            attachedObject.SetActive(false);
            AudioManager.PlayClipOnce(AudioManager.AudioID.PICKUPITEM);
        }
        else//already something in
        {
            GazeLabel.GetInstance().ShowTextForDuration("Use the inventory to store multiple items or combine them", 2);
            AudioManager.PlayClipOnce(AudioManager.AudioID.ERROR);
        }
       
    }

    private void SetSprite(Sprite s)
    {
        UIImageWhereToShow.enabled = true;
        UIImageWhereToShow.sprite = s;
        
    }
    public void Clear()
    {
        attachedObject = null;
        UIImageWhereToShow.sprite = null;
        UIImageWhereToShow.enabled = false;
       // attachedObject.SetActive(true);
    }
}
