﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeLabel : MonoBehaviour //Singleton
{
    private static GazeLabel instance;
    protected static float timeToShow;
    protected static float timeShowing;
    protected static Text textUIElement;
    public bool isClear = false;

    private GazeLabel() { }

    public static GazeLabel GetInstance()
    {
        if (instance == null)
        {
            //if not, set instance to this
            instance = new GazeLabel();
        }
        return instance;
    }

    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
        GameObject gazeLabel = GameObject.FindGameObjectWithTag("GazeLabel");
        textUIElement = gazeLabel.GetComponentInChildren<Text>() as Text;
        textUIElement.gameObject.SetActive(false);

    }
    // Use this for initialization


    // Update is called once per frame
    void Update()
    {
        timeShowing += Time.deltaTime;
        if(timeShowing > timeToShow  && !isClear)
        {
            Clear(); 
        }
    }

    public void ShowTextForDuration(string text, float sec)
    {
        isClear = false;
        timeShowing = 0;
        timeToShow = sec;
        if(textUIElement != null)
        {
            textUIElement.gameObject.SetActive(true);
            textUIElement.text = text;
        }
    }

    public void Clear()
    {
        timeToShow = 0;
        textUIElement.text = "";
        isClear = true;
        textUIElement.gameObject.SetActive(false);
    }
}
