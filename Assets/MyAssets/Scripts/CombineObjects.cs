﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombineObjects : MonoBehaviour //singleton class
{
    private static CombineObjects instance;

    private CombineObjects() { }

    public static CombineObjects GetInstance()
    {
        if(instance == null)
        {
            instance = new CombineObjects();
        }
        return instance;
    }  

    public GameObject Combine(ref GameObject  go1, ref GameObject go2)
    {
        GameObject combo = null;
        //syringe without needle and needle
        if((go1.tag == "SyringeNoNeedle" && go2.tag == "Needle") ||
            (go1.tag == "Needle" && go2.tag == "SyringeNoNeedle"))
        {
            var loadedObject = Resources.Load<GameObject>("SyringeEmptyInventory");
            combo = Instantiate(loadedObject );
        }
        //empty syringe with needle and flask
        else if ((go1.tag == "SyringeEmpty" && go2.tag == "Flask") ||
            (go1.tag == "Flask" && go2.tag == "SyringeEmpty"))
        {
            var loadedObject = Resources.Load<GameObject>("SyringeFullInventory");
            combo = Instantiate(loadedObject);
            if(go1.tag.Contains("Flask"))
            {
                combo.GetComponent<InventoryItem>().label = "Syringe containing " + go1.GetComponent<InventoryItem>().label.Substring(6);
            }
            else if (go2.tag.Contains("Flask"))
            {
                combo.GetComponent<InventoryItem>().label = "Syringe containing " + go2.GetComponent<InventoryItem>().label.Substring(6);
            }

        }
        return combo;
    }
}
