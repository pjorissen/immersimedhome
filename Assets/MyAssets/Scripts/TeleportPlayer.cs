﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform player;
    private float playerStartYPos;


   // private AudioManager audioMgr;
   //TODO FIX BUG DOESNT ALWAYS WORK????


    public void Start()
    {
        playerStartYPos = player.localPosition.y;
    }
    public void Teleport()
    {
        // player.position = new Vector3(transform.position.x, transform.position.y + playerStartYPos, transform.position.z);

        player.GetComponent<MoveToPosition>().SetGoalPos(new Vector3(transform.position.x, transform.position.y + playerStartYPos, transform.position.z));
    }

}
