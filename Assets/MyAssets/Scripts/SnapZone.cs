﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapZone : MonoBehaviour
{
    // Start is called before the first frame update
    
    private GazeSelectedObject gazeSelectedObjectScript = null;
    private bool isTaken = false;
    private Transform snapLocation;
    GameObject storedObject = null;
   
    void Start()
    {
        gazeSelectedObjectScript = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();
        snapLocation = this.transform.GetChild(0);
        isTaken = false;
    }


    public void SetGazeObject()
    {
        if (!isTaken)//handle empty gaze at
        {
            storedObject = gazeSelectedObjectScript.GetAttachedObject();
            if (storedObject)
            {
                storedObject.SetActive(true);
                storedObject.transform.position = snapLocation.transform.position;
                storedObject.transform.rotation = snapLocation.transform.rotation;
                gazeSelectedObjectScript.Clear();
                isTaken = true;
                AudioManager.PlayClipOnce(AudioManager.AudioID.STOREITEM);
            }
        }
        else
        {
            if (!gazeSelectedObjectScript.GetAttachedObject()) //HANDLE TAKING THINGS OUT AGAIN WHEN GAZE OBJECT IS EMPTY
            {
                storedObject.SetActive(true);
                gazeSelectedObjectScript.SetAttachedObject(storedObject);
                Clear();
                AudioManager.PlayClipOnce(AudioManager.AudioID.PICKUPITEM);
            }
            else//TODO HANDLE COMBINING OBJECTS
            {
                GameObject gazeObject = gazeSelectedObjectScript.GetAttachedObject();
                GameObject combo = CombineObjects.GetInstance().Combine(ref gazeObject, ref storedObject);
                if (combo != null)
                {
                    Destroy(gazeObject);
                    Destroy(storedObject);
                    storedObject = combo;
                    storedObject.SetActive(true);
                    storedObject.transform.position = snapLocation.transform.position;
                    storedObject.transform.rotation = snapLocation.transform.rotation;
                    gazeSelectedObjectScript.Clear();
                    isTaken = true;
                    AudioManager.PlayClipOnce(AudioManager.AudioID.COMBINEITEMS);
                }
                else
                {
                    GazeLabel.GetInstance().ShowTextForDuration("These items can not be combined", 2);
                    AudioManager.PlayClipOnce(AudioManager.AudioID.ERROR);
                }
            }
        }
    }

    public bool SetObject(GameObject newStored)
    {
        bool setSucces = false;
        if (!isTaken)//handle empty gaze at
        {
            storedObject = newStored;
            if (storedObject)
            {
                storedObject.SetActive(true);
                storedObject.transform.position = snapLocation.transform.position;
                storedObject.transform.rotation = snapLocation.transform.rotation;
                isTaken = true;
                AudioManager.PlayClipOnce(AudioManager.AudioID.STOREITEM);
                setSucces = true;
            }
        }
        return setSucces;
    }

    public void Clear()
    {
        storedObject = null;
        isTaken = false;
    }

    public string GetLabel()
    {
        if (isTaken && storedObject != null)
        {
            return (storedObject.GetComponent<InventoryItem>() as InventoryItem).label;
        }
        return "Snap objects here";
    }

    public bool IsTaken
    {
        get
        {
            return isTaken;
        }
    }
    public Transform GetSnapPosition(int index = 0)
    {
        if(index < this.transform.childCount)
        {
            return transform.GetChild(index);
        }
        return null;
    }
}
