﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : MonoBehaviour
{
    public Sprite sprite;
    public string label;
    private GazeSelectedObject selectedObject = null;
    // Start is called before the first frame update
    private void Start()
    {
        selectedObject = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();
    }

    public void IsGazeSelected()
    {
        if(selectedObject)
        {
            selectedObject.SetAttachedObject(this.gameObject);
            //already plays
           // AudioManager.PlayClipOnce(AudioManager.AudioID.PICKUPITEM);
        }
        else //not holding 
        {
            GazeLabel.GetInstance().ShowTextForDuration("You can store and combine items in here", 2);
            //AudioManager.PlayClipOnce(AudioManager.AudioID.ERROR);
        }
        
    }
}
