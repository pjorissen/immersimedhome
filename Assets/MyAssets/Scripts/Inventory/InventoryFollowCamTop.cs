﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryFollowCamTop : MonoBehaviour
{
    public float angleThreshHold;
    public GameObject objectToShowBelowThreshold;
    public Camera camera;
    public float stableTime = 1.0f;
    private float timeSinceLastDown;
    private bool isShowing;
    private Transform originalParent;
    private Vector3 originalLocalPos;
    private Quaternion originalLocalRot;
    public GameObject dummyParent;

    private float sinAngleThreshHold;
    public int ModuloFramesForOptimizing = 1;
    private int optimizeFrameCounter = 0;

    // Start is called before the first frame update
    void Start()
    {
        angleThreshHold = angleThreshHold * Mathf.Deg2Rad;
        sinAngleThreshHold = Mathf.Sin(angleThreshHold);
        timeSinceLastDown = 999999;
        isShowing = false;
        originalParent = objectToShowBelowThreshold.transform.parent;
        originalLocalPos = objectToShowBelowThreshold.transform.localPosition;
        originalLocalRot = objectToShowBelowThreshold.transform.localRotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        optimizeFrameCounter = (optimizeFrameCounter + 1) % ModuloFramesForOptimizing;
        timeSinceLastDown += Time.deltaTime;
        if (optimizeFrameCounter == 0)
        {
            //check if visible and show hide
            if (camera.transform.forward.y > sinAngleThreshHold)
            {
                timeSinceLastDown = 0;
            }

            if (timeSinceLastDown <= stableTime && !isShowing)
            {
                //startShowing
                objectToShowBelowThreshold.GetComponent<Canvas>().enabled = true;
                objectToShowBelowThreshold.GetComponent<CanvasScaler>().enabled = true;
                objectToShowBelowThreshold.GetComponent<GraphicRaycaster>().enabled = true;
                isShowing = true;
                //stop following camera;
                dummyParent.transform.position = camera.transform.position;
                dummyParent.transform.rotation = camera.transform.rotation;
                objectToShowBelowThreshold.transform.SetParent(dummyParent.transform);
            }
            else if (isShowing && timeSinceLastDown > stableTime)
            {
                //stop Showing
                objectToShowBelowThreshold.GetComponent<Canvas>().enabled = false;
                objectToShowBelowThreshold.GetComponent<CanvasScaler>().enabled = false;
                objectToShowBelowThreshold.GetComponent<GraphicRaycaster>().enabled = false;
                isShowing = false;
                objectToShowBelowThreshold.transform.SetParent(originalParent);
                objectToShowBelowThreshold.transform.localPosition = originalLocalPos;
                objectToShowBelowThreshold.transform.localRotation = originalLocalRot;
            }
        }
        


    }
}
