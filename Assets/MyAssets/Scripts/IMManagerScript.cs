﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Threading;

public class IMManagerScript : MonoBehaviour
{
    //always use first scenario, first patient and first medicine from the list
    private int curScenarionIndex = 0, curPatientIndex = 0, curMedIndex = 0;
    public static IMManagerScript instance = null;
    public MedicalAppData imData;
    public GameObject clipboardPrefab;
    public int defaultScenarioID = 0;
    public Scenario currScenario;
    public Text DebugText;

    public Patient currScenarioPatient;
    public Medicine currScenarioMedicine;
    public Patient selectedPatient;
    public Medicine selectedMedicine;
    public Medicine[] pickedUpMedicine;
    public int currScenarioAmountToAdminister;
    public string dataPathMobile = "IMData.xml";
    public string dataPathWindows = "Data/IMData.xml";

    public bool imDataLoaded = false;
    public bool simulationOver = false;
    public bool scenarioFinished = true;
    public bool correctPatientSelected = false;
    public bool correctMedicineSelected = false;
    public bool VANASCabinetLoggedIn = false;
    public bool administeredMedicine = false;
    public bool threwAwaySyringeEtcRightWay = false;
    public bool currWashedHands = true;
    public bool allwaysWashedHands = true; //set to false when at least forgot one time

    public int currScenarioNr = 0;

    public GameObject SimulationOver;
    public float delayForSimulationOverMenu = 1f;


    public Patient SelectedPatient
    {
        get
        {
            return selectedPatient;
        }
        set
        {
            selectedPatient = value;
            if (selectedPatient != null && selectedPatient == currScenarioPatient)
            {
                correctPatientSelected = true;
            }
            else
            {
                correctPatientSelected = false;
            }
        }
    }

    public Medicine SelectedMedicine
    {
        get
        {
            return selectedMedicine;           
        }
        set
        {
            selectedMedicine = value;
            if (SelectedMedicine != null && SelectedMedicine == currScenarioMedicine)
            {
                correctMedicineSelected = true;
            }
            else
            {
                correctMedicineSelected = false;
            }
        }
    }

    public TextMeshProUGUI scenarioSimulationOverMenu;
    public TextMeshProUGUI patientSimulationOverMenu;
    public TextMeshProUGUI medicineSimulationOverMenu;

    public TextMeshProUGUI medicineSimulationOverMenuOK;
    public TextMeshProUGUI medicineSimulationOverMenuNOK;
    public TextMeshProUGUI patientSimulationOverMenuOK;
    public TextMeshProUGUI patientSimulationOverMenuNOK;
    public TextMeshProUGUI alwaysWashedHandsSimulationOverMenuOK;
    public TextMeshProUGUI alwaysWashedHandsSimulationOverMenuNOK;
    public TextMeshProUGUI alwaysLoggedOffSimulationOverMenuOK;
    public TextMeshProUGUI alwaysLoggedOffSimulationOverMenuNOK;

    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += OnSceneLoaded;

        //SimulationOver.gameObject.SetActive(false);

        //show welcome message on label
        GazeLabel.GetInstance().ShowTextForDuration("Welcome to ImmersiMed @ Home", 3);
       
    }
    // Use this for initialization

    void Start()
    {
        string curPath = "";
        if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            curPath = dataPathWindows;
        }
        else if(Application.platform == RuntimePlatform.Android )
        {
            string path = Application.persistentDataPath;
            curPath= Path.Combine(path, dataPathMobile);
        }
        if(curPath.Length > 0)
        {
            imDataLoaded = readXml(curPath);
            DebugText.text = "IMData Loaded: " + imDataLoaded.ToString();
        }
        
        string msg = "Reading of IMData.xml succeeded: " + imDataLoaded;

        curScenarionIndex = imData.mScenarios[0].mID;
        currScenario = instance.imData.mScenarios[0]; //allways pick first scenario to start with
        curPatientIndex = imData.mPatients[0].mID;
        currScenarioPatient = imData.mPatients[0];
        curMedIndex = imData.mMedicines[0].mID;
        currScenarioMedicine = imData.mMedicines[0];
        if (imDataLoaded)
        {
            UpdateAlleClipboardTexts();
        }
        

        /*     //TEMP CREATE 
        // if (Application.platform == RuntimePlatform.Android)
        {
            string path = Application.persistentDataPath;
            
            path = Path.Combine(path, "IMData");
            if (!Directory.Exists(path) )
            {
                File.CreateText("imdata0.txt");
                path = Path.Combine(path, "IMDATA");
                Directory.CreateDirectory(path);
                File.CreateText(Path.Combine(path, "imdata1.txt"));
            }
        }*/
    }

    public void ResetStats()
    {

        //After leaving VanasRoom
        correctPatientSelected = false;
        correctMedicineSelected = false;
        VANASCabinetLoggedIn = false;

        administeredMedicine = false;
        threwAwaySyringeEtcRightWay = false;

        currWashedHands = true;
        allwaysWashedHands = true;

        SelectedMedicine = null;
        SelectedPatient = null;

        simulationOver = false;

        GameObject[] gos = GameObject.FindGameObjectsWithTag("SimulationOverMenu");
        for (int i = 0; i < gos.Length; i++)
        {
            //Debug.Log("Found " + gos[i].name);
            gos[i].SetActive(true);
        }

        if (GameObject.Find("medicineSimulationOverMenuOK"))
        {
            
            scenarioSimulationOverMenu = GameObject.Find("scenarioSimulationOverMenu").GetComponent<TextMeshProUGUI>();
            patientSimulationOverMenu = GameObject.Find("patientSimulationOverMenu").GetComponent<TextMeshProUGUI>();
            medicineSimulationOverMenu = GameObject.Find("medicineSimulationOverMenu").GetComponent<TextMeshProUGUI>();

            medicineSimulationOverMenuOK = GameObject.Find("medicineSimulationOverMenuOK").GetComponent<TextMeshProUGUI>();
            medicineSimulationOverMenuNOK = GameObject.Find("medicineSimulationOverMenuNOK").GetComponent<TextMeshProUGUI>();
            patientSimulationOverMenuOK = GameObject.Find("patientSimulationOverMenuOK").GetComponent<TextMeshProUGUI>();
            patientSimulationOverMenuNOK = GameObject.Find("patientSimulationOverMenuNOK").GetComponent<TextMeshProUGUI>();
            alwaysWashedHandsSimulationOverMenuOK = GameObject.Find("alwaysWashedHandsSimulationOverMenuOK").GetComponent<TextMeshProUGUI>();
            alwaysWashedHandsSimulationOverMenuNOK = GameObject.Find("alwaysWashedHandsSimulationOverMenuNOK").GetComponent<TextMeshProUGUI>();
            alwaysLoggedOffSimulationOverMenuOK = GameObject.Find("alwaysLoggedOffSimulationOverMenuOK").GetComponent<TextMeshProUGUI>();
            alwaysLoggedOffSimulationOverMenuNOK = GameObject.Find("alwaysLoggedOffSimulationOverMenuNOK").GetComponent<TextMeshProUGUI>();
        }
        
        if (GameObject.Find("SimulationOverMenu"))
        { 
            SimulationOver = GameObject.Find("SimulationOverMenu");
            SimulationOver.SetActive(false);
        }    
    }


    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("OnLevelWasLoaded");
        if (imDataLoaded)
        {
            UpdateAlleClipboardTexts();
        }
        ResetStats();
    }

    private void UpdateAlleClipboardTexts()
    {
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextScenario"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mScenarios[curScenarionIndex].mName;
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextPatient"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mPatients[curPatientIndex].mName;
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextMedicine"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mMedicines[curMedIndex].mClipboardName;
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextRoom"))
        {
            int roomNum = 401;
            switch(imData.mPatients[curPatientIndex].mType)
            {
                case PatientType.adult:
                    roomNum = 401;
                    break;
                case PatientType.child:
                    roomNum = 402;
                    break;
                case PatientType.pregnant:
                    roomNum = 403;
                    break;
                case PatientType.senior:
                    roomNum = 404;
                    break;
                default:
                    roomNum = 401;
                    break;
            }
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = roomNum.ToString();
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextSex"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mPatients[curPatientIndex].mSex.ToString();
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextAge"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mPatients[curPatientIndex].mAge.ToString();
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextWeight"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mPatients[curPatientIndex].mWeight.ToString();
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextType"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = imData.mPatients[curPatientIndex].mType.ToString();
        }
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("ClipboardTextAllergies"))
        {
            ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = "";
            foreach(string allergy in imData.mPatients[curPatientIndex].mAllergies)
            {
                ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text += allergy + ", ";
            }
            if (((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text.Length > 1)
            {
                ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text = ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text.Substring(0, ((TextMeshProUGUI)item.GetComponent<TextMeshProUGUI>()).text.Length - 2);
            }
        }

    }



    private bool readXml(string xmlFilePath)
    {
        return MedicalAppData.ReadFromFile(xmlFilePath, out imData);
    }

    public Medicine GetMedicineFromName(string name)
    {
        for (int i = 0; i < imData.mMedicines.Count; i++)
        {
            if (name == imData.mMedicines[i].mName)
            {
                return imData.mMedicines[i];
            }
        }
        return null;
    }

    public Patient GetPatientFromName(string name)
    {
        for (int i = 0; i < imData.mPatients.Count; i++)
        {
            if (name == imData.mPatients[i].mName)
            {
                return imData.mPatients[i];
            }
        }
        return null;
    }

    public Patient GetPatientFromRoomNumber(int roomNr)
    {
        for (int i = 0; i < imData.mPatients.Count; i++)
        {
            if (roomNr == imData.mPatients[i].mRoom)
            {
                return imData.mPatients[i];
            }
        }
        return null;
    }

    public void CheckForStats(Patient patient, Medicine medicine, float amountOfMedicineAdministered)
    {
        simulationOver = true;
        Debug.Log("Simulation over");
        WriteToFileManager.instance.AppendToFile("Simulation over, checking what happened:");
        SimulationOver.SetActive(true);

        scenarioSimulationOverMenu.text += "\n" + currScenario.mName;
        patientSimulationOverMenu.text += "\n" + currScenarioPatient.mName.Replace("/", " ");

        if (currScenarioAmountToAdminister == 1 && currScenarioMedicine.mUnit == Unit.units)
        {            
            medicineSimulationOverMenu.text += "\n" + currScenarioAmountToAdminister.ToString() + " unit " + currScenarioMedicine.mClipboardName;
        }
        else
        {     
            medicineSimulationOverMenu.text += "\n" + currScenarioAmountToAdminister.ToString() + " " + currScenarioMedicine.mUnit + " " + currScenarioMedicine.mClipboardName;
        }

        if (patient != currScenarioPatient)
        {
            //wrong patient
            patientSimulationOverMenuOK.gameObject.SetActive(false);
            patientSimulationOverMenuNOK.gameObject.SetActive(true);
            WriteToFileManager.instance.AppendToFile("Administered the medicine to the wrong patient: " + patient.mName.Replace("/", " "));
        }
        else
        {
            patientSimulationOverMenuOK.gameObject.SetActive(true);
            patientSimulationOverMenuNOK.gameObject.SetActive(false);
            WriteToFileManager.instance.AppendToFile("Administered the medicine to the right patient: " + patient.mName.Replace("/", " "));
        }

        if (medicine != currScenarioMedicine)
        {
            //wrong medicine
            medicineSimulationOverMenuOK.gameObject.SetActive(false);
            medicineSimulationOverMenuNOK.gameObject.SetActive(true);
            WriteToFileManager.instance.AppendToFile("Administered to the wrong medicine: " + medicine.mName);            
        }
        else
        {
            //right medicine
            if (amountOfMedicineAdministered == currScenario.mNumberOfUnitsToAdminister)
            {
                //right amount
                medicineSimulationOverMenuOK.gameObject.SetActive(true);
                medicineSimulationOverMenuNOK.gameObject.SetActive(false);
                WriteToFileManager.instance.AppendToFile("Administered to the right medicine and right amount: " + amountOfMedicineAdministered + " vs " + currScenario.mNumberOfUnitsToAdminister);
            }
            else
            {
                //wrong amount
                medicineSimulationOverMenuOK.gameObject.SetActive(false);
                medicineSimulationOverMenuNOK.gameObject.SetActive(true);
                WriteToFileManager.instance.AppendToFile("Administered to the right medicine but wrong amount: " + amountOfMedicineAdministered + " vs " + currScenario.mNumberOfUnitsToAdminister);
            }

           
        }

        if (!allwaysWashedHands)
        {
            alwaysWashedHandsSimulationOverMenuOK.gameObject.SetActive(false);
            alwaysWashedHandsSimulationOverMenuNOK.gameObject.SetActive(true);
            WriteToFileManager.instance.AppendToFile("Not always washed hands when entering a patient room.");
        }
        else
        {
            alwaysWashedHandsSimulationOverMenuOK.gameObject.SetActive(true);
            alwaysWashedHandsSimulationOverMenuNOK.gameObject.SetActive(false);
            WriteToFileManager.instance.AppendToFile("Always washed hands when entering a patient room.");
        }

        if (VANASCabinetLoggedIn)
        {
            alwaysLoggedOffSimulationOverMenuOK.gameObject.SetActive(false);
            alwaysLoggedOffSimulationOverMenuNOK.gameObject.SetActive(true);
            WriteToFileManager.instance.AppendToFile("Still logged in into VANAS cabinet.");
        }
        else
        {
            alwaysLoggedOffSimulationOverMenuOK.gameObject.SetActive(true);
            alwaysLoggedOffSimulationOverMenuNOK.gameObject.SetActive(false);
            WriteToFileManager.instance.AppendToFile("Correctly logged off.");
        }

    }

    public void CheckForStatsWhenLeavingVanasRoom()
    {
        if (!simulationOver)
        {
            if (SelectedPatient != null && SelectedMedicine != null && SelectedPatient.mName != "John Doe" && selectedMedicine.mName != "")
            {
                if (!VANASCabinetLoggedIn && correctMedicineSelected && correctPatientSelected)
                {
                    //all good, carry on
                    Debug.Log("All good, carry on");
                    WriteToFileManager.instance.AppendToFile("Correct patient, correct medicine, logged out, so all good.");
                }
                else
                {
                    CheckForStats(SelectedPatient, SelectedMedicine, currScenario.mNumberOfUnitsToAdminister);   //so always correct amount, we didn't administer anything yet                 
                }
            }
            else if (VANASCabinetLoggedIn) //logged in but did not select patient or medicine, but did leave room while loogged in
            {

                simulationOver = true;
                //game over, something wrong
                Debug.Log("Simulation over, something wrong");
                WriteToFileManager.instance.AppendToFile("Simulation over, something wrong");

                SimulationOver.SetActive(true);

                patientSimulationOverMenuOK.gameObject.SetActive(false);
                patientSimulationOverMenuNOK.gameObject.SetActive(false);
                medicineSimulationOverMenuOK.gameObject.SetActive(false);
                medicineSimulationOverMenuNOK.gameObject.SetActive(false);
                alwaysLoggedOffSimulationOverMenuOK.gameObject.SetActive(false);
                alwaysLoggedOffSimulationOverMenuNOK.gameObject.SetActive(true);
                alwaysWashedHandsSimulationOverMenuOK.gameObject.SetActive(false);
                alwaysWashedHandsSimulationOverMenuNOK.gameObject.SetActive(false);
                WriteToFileManager.instance.AppendToFile("Not logged out correctly");
                
            }
            else
            {
                //scenario still in progress
                //Debug.Log("Scenario still in progress.");
                //WriteToFileManager.instance.AppendToFile("Scenario still in progress.");
            }
        }
    }

    public void CheckForStatsWhenAdministeredMedicine(Patient patient, Medicine administeredMedicine, float amountOfMedicineAdministered)
    {
        CheckForStats(patient, administeredMedicine, amountOfMedicineAdministered);
    }

    

}


