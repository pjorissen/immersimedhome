﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanClipboard : MonoBehaviour
{
    public SnapZone medsSpawnZone;
    // Start is called before the first frame update
    public GameObject boxPrefab;
    public GameObject flaskPrefab;
    public GameObject bottlePrefab;
    public GameObject infusion_bagPrefab;
    public GameObject tabletPrefab;//single pill
    public GameObject capsulePrefab;//single pill
    public GameObject capletPrefab;//single pill
    private GazeSelectedObject gazeSelectedObject;

    public void Start()
    {
        gazeSelectedObject = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();

    }

    public string GetLabel()
    {
        if(medsSpawnZone.IsTaken)
        {
            return "Pick up medication from the medication zone please";
        }
        else
        {
            return "Scan prescription clipboard to retrieve medication";
        }
        
    }

    public void SpawnMedicineIfConditionsOK()
    {
        //TODO check if holding prescription in gaze
        if(gazeSelectedObject.GetAttachedObject() != null && gazeSelectedObject.GetAttachedObject().tag ==("ClipboardInventory"))
        {
            if (!medsSpawnZone.IsTaken)
            {
                Medicine med = IMManagerScript.instance.currScenarioMedicine;
                GameObject newMedObject = null;
                switch (med.mPackage)
                {
                    case Package.box:
                        {
                            newMedObject = GameObject.Instantiate(boxPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Box: " + med.mClipboardName;
                            }
                            break;
                        }
                    case Package.flask:
                        {
                            newMedObject = GameObject.Instantiate(flaskPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Flask: " + med.mClipboardName;
                            }
                            break;
                        }
                    case Package.bottle:
                        {
                            newMedObject = GameObject.Instantiate(bottlePrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Bottle: " + med.mClipboardName;
                            }
                            break;
                        }
                    case Package.infusion_bag:
                        {
                            newMedObject = GameObject.Instantiate(infusion_bagPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "IV Bag: " + med.mClipboardName;
                            }
                            break;
                        }
                    case Package.tablet://single pill
                        {
                            newMedObject = GameObject.Instantiate(tabletPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Tablet: " + med.mClipboardName;
                            }
                            break;
                        }
                    case Package.capsule://single pill
                        {
                            newMedObject = GameObject.Instantiate(capsulePrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Capsule: " + med.mClipboardName;
                            }
                            break;
                        }
                    case Package.caplet://single pill
                        {
                            newMedObject = GameObject.Instantiate(capletPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Caplet: " + med.mClipboardName;
                            }
                            break;
                        }
                    default:
                        {
                            newMedObject = GameObject.Instantiate(boxPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity);
                            InventoryItem invItem = newMedObject.GetComponent<InventoryItem>();
                            if (invItem)
                            {
                                invItem.label = "Box: " + med.mClipboardName;
                            }
                            break;
                        }
                }
                //set InventoryItem name
                
                //add to Snapzone
                if (!medsSpawnZone.SetObject(newMedObject))
                {
                    GazeLabel.GetInstance().ShowTextForDuration("Your medicine could not be delivered", 2);
                    AudioManager.PlayClipOnce(AudioManager.AudioID.ERROR);
                }
                else
                {
                    GazeLabel.GetInstance().ShowTextForDuration("Your medicine has been delivered", 2);
                    AudioManager.PlayClipOnce(AudioManager.AudioID.SPAWNITEM);
                }
            }
            else
            {
                GazeLabel.GetInstance().ShowTextForDuration("Remove medication from the medication zone please", 2);
                AudioManager.PlayClipOnce(AudioManager.AudioID.ERROR);
            }
        }    
    }
}
