﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrab : MonoBehaviour
{
    public GameObject handObject;
    public GameObject objectGrabbed;

    private bool emptyHands = true;
    private Vector3 origLocalPos;
    private Transform origParent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            if(emptyHands)
            {
                origParent = objectGrabbed.transform.parent;
                origLocalPos = objectGrabbed.transform.localPosition;
                objectGrabbed.transform.SetParent(handObject.transform);
                objectGrabbed.transform.localPosition = handObject.transform.localPosition;
                emptyHands = !emptyHands;
            }
            else
            {
                //only if you move it out of the cursors field
                //this.GetComponent<PlayerGrab>().enabled = false;
                objectGrabbed.transform.SetParent(origParent);
                objectGrabbed.transform.localPosition = origLocalPos;
                emptyHands = !emptyHands;
            }
        }
    }
}
