﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalk : MonoBehaviour
{
    public Vector3 walkSpeed;
    private Vector3 movement = new Vector3(0, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButton("Fire1"))
        {
            movement.x = Camera.main.transform.forward.x * walkSpeed.x;
            movement.y = Camera.main.transform.forward.y * walkSpeed.y;
            movement.z = Camera.main.transform.forward.z * walkSpeed.z;
            transform.position = transform.position + movement * Time.deltaTime;
        }
    }
}
