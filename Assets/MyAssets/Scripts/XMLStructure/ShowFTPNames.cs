﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FluentFTP;

public class ShowFTPNames : MonoBehaviour
{
    public Text textField;
    // Start is called before the first frame update
    void Start()
    {
        FtpListItem[] items = MyFtpClient.Instance.GetFileListing("");
        if(items != null)
        {
            textField.text = "";
            foreach (FtpListItem item in items)
            {
                textField.text += item.FullName + "\n";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
