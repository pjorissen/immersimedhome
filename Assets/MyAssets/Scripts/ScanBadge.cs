﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanBadge : MonoBehaviour
{
    private GazeSelectedObject gazeSelectedObject;

    public void Start()
    {
        gazeSelectedObject = (GazeSelectedObject)GameObject.FindGameObjectWithTag("SelectedGazeTargetImage").GetComponent<GazeSelectedObject>();

    }

    public string GetLabel()
    {
        if(true)//todo: check log on off from vanas 
        {
            return "Scan badge to log on/off";
        }
        
    }

    public void LogOnOff()
    {
        if (gazeSelectedObject.GetAttachedObject() != null && gazeSelectedObject.GetAttachedObject().tag == ("Badge"))
        {
            //todo log on or off vanas kast + waitfor seconds
            if (true)//todo: check log on off from vanas 
            {

                GazeLabel.GetInstance().ShowTextForDuration("Logged on/off Vanas", 2);
            }
        }
        else
        {
            GazeLabel.GetInstance().ShowTextForDuration("Scan badge to log on/off Vanas", 2);
        }
    }

}
