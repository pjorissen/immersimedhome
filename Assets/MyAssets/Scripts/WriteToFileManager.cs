﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class WriteToFileManager : MonoBehaviour
{

    public static WriteToFileManager instance = null;

    public string folderName = "PerformanceLogs";

    public string fileName = "";

    bool simulationStarted = false;

    public int maxNrOfFilesAllowed = 25;

    private void Awake()
    {
        //Debug.Log("Starting ImmersiMed");
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
            if (Directory.Exists(folderName))
            //check for right folder
            {
                //Debug.Log("Folder " + folderName + " exists");
                //folder exists, delete all but the newest
                foreach (var fi in new DirectoryInfo(folderName).GetFiles().OrderByDescending(x => x.LastWriteTime).Skip(maxNrOfFilesAllowed))
                    fi.Delete();
            }
            else
            {
                //if doesn't exist, make it
                //Debug.Log("Folder " + folderName + " does not exist, creating it");
                Directory.CreateDirectory(folderName);
            }

            fileName = "Simulation Log " + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".csv";
            File.Create(folderName + "/" + fileName);
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }  
    }

    // Use this for initialization
    void Start()
    {
        AppendToFile("Starting ImmersiMed");
       
        simulationStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Time.frameCount % 60 == 0) Just testing some stuff
        {
            AppendToFile("Ongoing simulation at time " + Time.time);
        }
        */
    }

    public void AppendToFile(string strToAppend)
    {
        //append to file
   /*     strToAppend = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "," + strToAppend;

        using (FileStream fs = new FileStream(folderName + "/" + fileName, FileMode.Append, FileAccess.Write))
        using (StreamWriter sw = new StreamWriter(fs))
        {
            sw.WriteLine(strToAppend);
        }

        //for unity debug purposes
        Debug.Log(strToAppend);*/
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            AppendToFile("Pausing simulation");
        }
        else if(simulationStarted)
        {
            AppendToFile("Resuming simulation");
        }
    }

    private void OnDisable()
    {
        AppendToFile("Ending ImmersiMed");
    }
}
