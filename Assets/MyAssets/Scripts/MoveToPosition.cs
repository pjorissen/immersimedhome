﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPosition : MonoBehaviour
{
    // Start is called before the first frame update
    protected Vector3 startPos;
    protected Vector3 goalPos;
    public float timeToGetThere = 1f;

    void Start()
    {
        startPos = transform.position;
        goalPos = startPos;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetGoalPos(Vector3 newPos)
    {
        goalPos = newPos;
        startPos = transform.position;
        AudioManager.StartClipLoop(AudioManager.AudioID.FOOTSTEPS);
        StartCoroutine(WaitAndMove(timeToGetThere));
    }

    IEnumerator WaitAndMove(float timeToMove)
    {
        float startTime = Time.time; // Time.time contains current frame time, so remember starting point
        while (Time.time - startTime <= timeToMove)
        { // until one second passed
            transform.position = Vector3.Lerp(startPos, goalPos, (Time.time - startTime)/timeToMove ); // lerp from A to B in one second
            yield return 1; // wait for next frame
        }
        AudioManager.StopClipLoop(AudioManager.AudioID.FOOTSTEPS);
    }
}
