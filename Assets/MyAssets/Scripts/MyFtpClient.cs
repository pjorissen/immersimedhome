﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FluentFTP;
using System.Net;
using System;

public class MyFtpClient : MonoBehaviour
{
    private static MyFtpClient instance = null;
    public string host = "immersimed-unidat.kdg.be";
    public string login = "immersimed-ud-ftpapp";
    public string password = "ArmbandReclameBal201911";
    private FtpClient client;

    // Game Instance Singleton
    public static MyFtpClient Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {


                // get a list of files and directories in the "/htdocs" folder
                foreach (FtpListItem item in GetFileListing(""))
                {
                    Debug.Log("File: " + item.FullName);
               
            }
           
    }


// Update is called once per frame
void Update()
    {
        
    }

    private static void OnValidateCertificate(FtpClient control, FtpSslValidationEventArgs e)
    {
        if (e.PolicyErrors != System.Net.Security.SslPolicyErrors.None)
        {
            // invalid cert, do you want to accept it?
             e.Accept = true;
        }
    }

    public bool UploadFile(string localPath, string serverPath)
    {
        bool succes = false;
        try
        {
           if(Connect())
            {
                client.UploadFile(localPath, serverPath);
            }
            succes = true;
        }
        catch (Exception ex)
        {
            Debug.Log("FTP upload error: " + ex.Message);
        }
        finally
        {
            Disconnect();
        }
        return succes;
    }
    public bool DownloadFile(string localPath, string serverPath)
    {
        bool succes = false;
        try
        {
            if (Connect())
            {
                client.DownloadFile(localPath, serverPath);
            }           
            succes = true;
        }
        catch (Exception ex)
        {
            Debug.Log("FTP upload error: " + ex.Message);
        }
        finally
        {
            Disconnect();
        }
        return succes;
    }

    public FtpListItem[] GetFileListing(string serverPath)
    {
        FtpListItem[] result = null;
        try
        {
            if (Connect())
            {
                result = client.GetListing(serverPath);
            }            
        }
        catch (Exception ex)
        {
            Debug.Log("FTP listing error: " + ex.Message);
        }
        finally
        {
            Disconnect();
        }
        return result;
    }

    private bool Connect()
    {
        try
        {
            client = new FtpClient();
            client.Host = host;
            client.Credentials = new NetworkCredential(login, password);
            client.EncryptionMode = FtpEncryptionMode.Explicit;
            client.ValidateCertificate += new FtpSslValidation(OnValidateCertificate);
            client.Connect();
        }
        catch(Exception e)
        {
            Debug.Log("FTP Connection Error: " + e.Message);
        }
        return client.IsConnected;
    }
    private void Disconnect()
    {
        try
        {           
            client.Disconnect();
        }
        catch (Exception e)
        {
            Debug.Log("FTP Disconnection Error: " + e.Message);
        }
    }
}
